<?php

$cache = $container->get('settings')['cache'];
if (array_key_exists('useCache', $cache) && $cache['useCache']) {
    $container['cache'] = static function () {
        return new \Slim\HttpCache\CacheProvider();
    };
}
