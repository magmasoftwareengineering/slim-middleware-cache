<?php

return [
    'cache' => [
        'useCache' => $_ENV['MODULE_HTTP_CACHE_USE_CACHE'] ?? false,
        'cacheControl' => $_ENV['MODULE_HTTP_CACHE_CONTROL'] ?? 'public',
        'maxAge' => $_ENV['MODULE_HTTP_CACHE_MAX_AGE'] ?? 86400,
        'mustRevalidate' => $_ENV['MODULE_HTTP_CACHE_REVALIDATE'] ?? false,
    ]
];
