<?php

use \Slim\HttpCache\Cache;

/** @var \Slim\Container $container */
/** @var \Slim\Collection $cache */
$cache = $settings['cache'];
if (array_key_exists('useCache', $cache) && $cache['useCache'] && array_key_exists('cacheControl', $cache) &&
    array_key_exists('maxAge', $cache) && array_key_exists('mustRevalidate', $cache)) {
    $app->add(new Cache($cache['cacheControl'], $cache['maxAge'], $cache['mustRevalidate']));
}
